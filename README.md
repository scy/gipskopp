# Gipskopp

This is some code that I used for measuring the moisture level of a wall in my apartment.

It’s late and I don’t feel like writing a detailed readme.
Check out the thread I wrote about this project for details, either [on Mastodon](https://mastodon.scy.name/@scy/107038266730005373) or [on Twitter](https://twitter.com/scy/status/1444683004018835465).

The project consists of two parts:
A MicroPython one in [`main.py`](main.py) to run on an ESP32 microcontroller, and companion Python code in [`gipscloud.py`](gipscloud.py).

## The MicroPython part

This part will run on the microcontroller, measure the voltage coming across a voltage divider involving the wall, and publish voltage and the derived resistance to an MQTT server.

The code is expecting you to use pin 34 as the analog input.

Configuration is done by adding a file called `config.json` to the root of the board.
It needs to have the following fields set:

* `wlan_ssid`: Name of the network to connect to.
* `wlan_pw`: Wi-Fi password.
* `mqtt_host`: Host name or IP address to connect to.
* `mqtt_topic`: The MQTT topic you’d like to have your values published too.

There are the following optional fields:

* `hostname`: MQTT client ID and DHCP host name the controller should use. Defaults to `gipskopp`.
* `r1`: Resistance of the resistor on the breadboard, in Ohms. Defaults to `470000` (470 kΩ). The wall is R₂. :)
* `u`: The total input voltage on the divider. Defaults to `3.3` and you can use the `3V3` header on your board.

There’s no support for MQTT authentication because this is a hacky setup.

The code is using a watchdog and will reboot after 35 seconds if something goes wrong (e.g. no MQTT connection).

In my experience the ADC of the ESP32 is all over the place if you don’t use a filter circuit, therefore there is software smoothing on the values:
The voltage cannot change more than about 0.02 V per second.
This should protect us from spikes to a degree.

## The Python part

This will connect to the MQTT server, subscribe to the values and send them to a Graphite server every five seconds.
It requires the `paho-mqtt` and `requests` packages to be installed.

This part is configured using environment variables, all of which are required:

* `MQTT_HOST`: Host name or IP address to connect to. We don’t do authentication here either.
* `MQTT_TOPIC`: The topic to subscribe to. Set this to the same value as `mqtt_topic` on the controller.
* `GRAPHITE_METRIC`: How should the Graphite metric be called? `.resistance` is added to this value, i.e. if you set it to `environment.gipskopp`, the resulting metric name is `environment.gipskopp.resistance`.
* `GRAPHITE_URL`: The URL to `POST` to. I’m using Grafana Cloud; their URL looks something like `https://graphite-prod-01-eu-west-0.grafana.net/graphite/metrics`.
* `GRAPHITE_APIKEY`: Will be used as a bearer token. For Grafana Cloud, this is something like `203070:eyJrIjoiN<a_long_base64_string>…`.
