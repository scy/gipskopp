import json
from os import environ as env
from time import monotonic, time

import paho.mqtt.client as mqtt_client
import requests


send_interval = 5
last_send = monotonic()
messages = []


def on_connect(client, userdata, flags, rc):
    client.subscribe(env["MQTT_TOPIC"])


def on_message(client, userdata, msg):
    global last_send, messages
    try:
        data = json.loads(msg.payload)
        now = monotonic()
        message = {
            "name": "%s.resistance" % env["GRAPHITE_METRIC"],
            "value": data["resistance"],
            "interval": 1,
            "time": round(time()),
        }
        messages.append(message)
        if last_send + send_interval <= now:
            messages.sort(key=lambda msg: msg["time"])
            print(messages)
            res = requests.post(
                env["GRAPHITE_URL"],
                headers={
                    "Authorization": "Bearer %s" % env["GRAPHITE_APIKEY"],
                },
                json=messages,
            )
            messages = []
            last_send = now
            print(res, res.content)
    except:
        pass


mqtt = mqtt_client.Client()
mqtt.on_connect = on_connect
mqtt.on_message = on_message

mqtt.connect(env["MQTT_HOST"])
mqtt.loop_forever()
