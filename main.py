import json
from machine import ADC, Pin, WDT
import network
import time
from umqtt.simple import MQTTClient


# Start the watchdog.
wdt = WDT(timeout=35_000)

# Read config.
with open("config.json", "rb") as cfile:
    config = json.load(cfile)

hostname = config.get("hostname", "gipskopp")
r1 = config.get("r1", 470_000)  # First resistor value in Ohms.
u  = config.get("u", 3.3)       # Input voltage to measuring circuit.

# Start network.
nic = network.WLAN(network.STA_IF)
nic.active(True)
nic.config(dhcp_hostname=hostname)
nic.connect(config["wlan_ssid"], config["wlan_pw"])
# Wait until connected. We're not feeding the watchdog in the loop on purpose.
while not nic.isconnected():
    time.sleep(1)
wdt.feed()

# Fire up MQTT.
mqtt = MQTTClient(hostname, config["mqtt_host"], keepalive=30)
mqtt.connect(clean_session=True)
wdt.feed()

# Set up the ADC.
adc = ADC(Pin(34))
adc.atten(ADC.ATTN_11DB)  # Use full 3.3V range.

# The ADC seems to be influenced by the Wi-Fi or power supply or both.
# Let's smooth things out by allowing only a gradual change in voltage.
max_reading_diff = 400  # about 0.02 V per interval
prev_reading = None

while True:
    adc_reading = adc.read_u16()
    smoothed = False
    if prev_reading is not None:
        diff = adc_reading - prev_reading
        new_adc_reading = prev_reading + max(min(diff, max_reading_diff), -max_reading_diff)
        if new_adc_reading != adc_reading:
            smoothed = True
        print(adc_reading, new_adc_reading, smoothed)
        adc_reading = new_adc_reading
    prev_reading = adc_reading
    u2 = adc_reading * 3.3 / 65535
    u1 = u - u2
    r2 = 100_000_000 if u1 == 0 else round(r1 * u2 / u1)

    # Tend to the socket.
    mqtt.check_msg()

    print("{0:0.3f}V {1}O {2}".format(u2, r2, "s" if smoothed else " "))

    # Send to MQTT.
    mqtt.publish(config["mqtt_topic"], json.dumps({
        "voltage": u2,
        "resistance": r2,
        "smoothed": smoothed,
    }))

    wdt.feed()
    time.sleep(1)
